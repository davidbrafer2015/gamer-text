import java.util.Random;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		
		Scanner in = new Scanner(System.in);
		
		String nome;
		
		Random rand = new Random();
		
		int dano = rand.nextInt(100);
		
		System.out.println("Insira um NickName: ");
		nome = in.nextLine();
		System.out.println("Bem-Vindo ao Jogo: "+nome);
		System.out.println("VAMOS ENTRAR NA CASA DOS HORROES");
		System.out.println("Escolha a Porta que deseja entrar: 1,2,3,4");
		String comando = in.nextLine();
		
		if(comando.equals("1")) {
		
			System.out.println("Entrando na Porta 1!!");
			System.out.println("Inimigo vindo na sua direção o que deseja fazer? | A = Atacar | B = Correr |");
			comando = in.nextLine();
				
				if(comando.equals("a")) {
					
					if(dano>75) {
						
						System.out.println("Você derrotou o inimigo |Dano| "+dano );
						System.out.println("Vamos para a próxima porta: |Porta = 1|Porta =2 |");
						comando = in.nextLine();
						
						if(comando.equals("1")) {
							
							System.out.println("Você entrou numa sala e acabou sendo incinerado!|GAME OVER|");
							
						}else if(comando.equals("2")) {
							
							System.out.println("Apareceu um ZUMBIIII! |A = Atacar|");
							comando = in.nextLine();
							
							if(comando.equals("a")) {
								
								if(dano>75) {
									
									System.out.println("Você matou o ZUMBII e passou pela ultima porta. Prabéns você VENCEUU! "+nome);
									System.out.println("Vamos ver se você tem essa sorte novamente ?");
									
								}else {
									
									System.out.println("O ZUMBI te devorou!|GAME OVER|");
								}
							}
					
						}
												
					}else {
						
						System.out.println(dano+" de dano e acabou te matando ");
						System.out.println("!!Game Over!!");
						
					}
					
				}else if(comando.equals("b")) {
					
					System.out.println("Você conseguiu fugir!!");
					System.out.println("Você caiu num buraco e encontrou uma caverna e um caminho escuro, o que deseja fazer ?? | A = Entrar na caverna. | B = Caminho Escuro. |");
					comando = in.nextLine();
					
						if(comando.equals("a")) {
							
							System.out.println("Você encontrou um Dragão!!!!|GAME OVER|");
							
						}else if(comando.equals("b")) {
							
							System.out.println("Você encontrou o tesouro!!FIM do JOGO! "+nome);
							System.out.println("Vamos ver se você tem essa sorte novamente ?");
						}
						
				}	
				
		}else if(comando.equals("2")) {
			
			System.out.println("Você encontrou um bilhete!!");
			System.out.println("-------------------------------------------------");
			System.out.println("Em sua frente tem duas caixas, uma dourada e outra prata.Escolha com sabedoria pois nem tudo que reluz é ouro e que algumas vezes a prata vale mais que o ouro!");
			System.out.println("-------------------------------------------------");
			
			System.out.println("Ande mais um pouco... | A = Caixa de Ouro | B = Caixa Prata |");
			comando = in.nextLine();
				
				if(comando.equals("a")) {
					
					System.out.println("Saiu um gás da caixa dourada, mortal para quem respira!");
					System.out.println("-------------------------------------------------");
					System.out.println("GAME OVER");
					System.out.println("-------------------------------------------------");
					comando = in.nextLine();
					
				}else if(comando.equals("b")) {
					
					System.out.println("Parabéns, sáiba escolha... Pegue esta chave e escolha uma porta");
					System.out.println("-------------------------------------------------");
					System.out.println("|Porta 1 | Porta 2|");
					comando = in.nextLine();
					
					if(comando.equals("1")) {
						
						System.out.println("Você caiu em um tanque cheio de enguias elértricas...");
						System.out.println("-------------------------------------------------");
						System.out.println("GAME OVER");
						System.out.println("-------------------------------------------------");
						
					}else if(comando.equals("2")) {
						
						System.out.println("A  sala está vazia e so tem uma porta a frente...");
						System.out.println("-------------------------------------------------");
						System.out.println("Está trancada...");
						System.out.println("-------------------------------------------------");
						System.out.println("A = Usar a chave!");
						comando = in.nextLine();
						
						if(comando.equals("a")) {
							
							System.out.println("Vencedooor, saiu da CASA DOS HORRORES "+nome);
							System.out.println("Vamos ver se você tem essa sorte novamente ?");
						}else {
							
							System.out.println("Opção Inválida");
						}
					}
			}
		}else if(comando.equals("3")) {
			
			System.out.println("A sala está escura e....");
			System.out.println("Acênde uma luz negra e mostra uma charada na parade...");
			System.out.println("Dois Homens estão no deserto.Ambos estão com uma mochila nas costas.A mochila de um está aberta e vazia.A mochila do outro está fechada e guarda alguma coisa.Um dos homens está morto. O que há dentro da mochila fechada?");
			System.out.println("Caso você erre a charada você morrerá multilado...");
			System.out.println("|A = Um Paraquedas|B = Suprimentos|");
			comando = in.nextLine();
			
			if(comando.equals("a")) {
				
				System.out.println("ACERTOU.... Escolha uma Porta...|Porta = 1|Porta = 2|");
				comando = in.nextLine();
					
				if(comando.equals("1")) {
					
						System.out.println("Mais uma charada...");
						System.out.println("O que é, o que é?...Na cidade é uma profissão, na estrada é um perigo e na mata é um inseto...");
						System.out.println("Digite sua resposta...");
						comando = in.nextLine();
						
						if(comando.equals("barbeiro")) {
							
							System.out.println("Você é bom nas charadas...");
							System.out.println("Vamos seguir adiante...");
							System.out.println("Sua próxima escolha pode te levar a MORTE ou a VITÓRIA...");
							System.out.println("|Porta = A|Porta = B|");
							comando = in.nextLine();
							
							if(comando.equals("a")) {
								
								System.out.println("Boa escolha, você acaba de encontrar uma mala cheia de ouro e a porta para saída aberta!!");
								System.out.println("Vencedor!!!! "+nome);
								System.out.println("Vamos ver se você tem essa sorte novamente ?");
								
							}else if(comando.equals("b")) {
								
								System.out.println("Ao abrir a porta você leva um tiro no meio da cabeça!!");
								System.out.println("GAME OVER");
								
							}
						}else {
							
							System.out.println("Voce é devorado por um ataque de gafanhotos assasinos!!!");
							System.out.println("GAME OVER");
						}
						
				}else if(comando.equals("2")) {
					
					System.out.println("Leões famíntos estão te devorando ao entrar");
					System.out.println("Game Over");
				}
			}else if(comando.equals("b")) {
				
				System.out.println("Leões famíntos estão te devorando ao entrar");
				System.out.println("Game Over");
				
			}
		}else if(comando.equals("4")) {
			
			System.out.println("Você acaba de entrar na sala aonde você terá que contar com a sorte...");
			System.out.println("Logo em seguinda surgi um zumbi...");
			System.out.println("------------------------------------");
			System.out.println("A = Atirar | B = Chutar");
			comando = in.nextLine();
			
			if(comando.equals("a")) {
				if(dano<50) {
					
					System.out.println("O Zumbi te matou!!!");
					System.out.println("Game Over!!");
					
				}else if(dano>50) {
					
					System.out.println("Você matou o Zumbi...");
					System.out.println("Vamos avançar");
					System.out.println("Escolha uma Porta...");
					System.out.println("Porta A || Porta B");
					comando = in.nextLine();
					
					if(comando.equals("a")) {
						
						System.out.println("Você é atingido por flechas na cabeça!!");
						System.out.println("GAME OVER");
						
					}else if(comando.equals("b")) {
						
						System.out.println("Nessa sala você encontra 3 cobras aonde...");
						System.out.println("e você tem duas caixas");
						System.out.println("Ambas com armas...");
						System.out.println("Porém uma está sem munição.");
						System.out.println("Caixa 1|Caixa 2");
						comando = in.nextLine();
						
						if(comando.equals("1")) {
							
							System.out.println("Tem duas pistolas, ambas descarregadas...");
							System.out.println("As cobras pulam em você ...");
							System.out.println("Você morre com o veneno das cobras...");
							System.out.println("GAME OVER");
							
						}else if(comando.equals("2")) {
							
							System.out.println("Tem uma espinguarda totalmente carregada...");
							System.out.println("você atira e ...");
							
							if(dano>50) {
								
								System.out.println("Você matou as três cobras com um único tiro..");
								System.out.println("Agora podemos seguir...");
								System.out.println("Na sua frente tem uma porta e um escorrego...");
								System.out.println("Abrir a porta = 1| Pular no escorrego = 2");
								comando = in.nextLine();
								
								if(comando.equals("1")) {
									
									System.out.println("Ao abrir a porta acontece uma EXPLOSÃOOO");
									System.out.println("Você fica em pedacinhos...");
									System.out.println("GAME OVER");
									
								}else if(comando.equals("2")) {
									
									System.out.println("Você saiu da casa...");
									System.out.println("Parabéns...");
									System.out.println("Você venceu "+nome);
									System.out.println("Vamos ver se você tem essa sorte novamente ?");
								}
								
							}else {
								
								System.out.println("Você matou uma e as outras duas te envenenaram...");
								System.out.println("GAME OVER!!");
							}
						}
					}
					
				}
			}
			
		}
		
	}
}
